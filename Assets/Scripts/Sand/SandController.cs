using System.Runtime.InteropServices;
using Unity.Mathematics;
using UnityEngine;

namespace Sand
{
    struct Particle
    {
        public int linkedIndex;
        public uint flags;
        public float2 position;
        public float2 velocity;
    }

    public class SandController : MonoBehaviour
    {
        // COMPONENT FIELDS
        [SerializeField] private ComputeShader computeShader;
        [SerializeField] private Mesh mesh;
        [SerializeField] private Material material;
        [SerializeField] private uint count;
        [SerializeField] private float particleSize;

        // GPU MEMORY BUFFERS
        private ComputeBuffer _particleBuffer;
        private ComputeBuffer _mapBuffer;
        private ComputeBuffer _argsBuffer;

        // SIZE OF SPACIAL PARTITIONING MAP
        private readonly int _mapSize = 32;
        // INSTANCED MESH SHADER ARGUMENT BUFFER
        private uint[] _args = new uint[5];

        private static readonly Bounds Bounds = new Bounds(new Vector3(0, 0, 0), new Vector3(100, 100, 100));

        // COMPUTE SHADER KERNEL INDEX
        private enum KernelIDs
        {
            InitializeParticles = 0,
            InitializeMap   = 1,
            UpdateMap   = 2,
            UpdateParticles = 3,
        }

        // CACHED SHADER PROPERTY IDENTIFIERS
        private static readonly int MapSizePropertyID =         Shader.PropertyToID("_MapSize");
        private static readonly int CountPropertyID =           Shader.PropertyToID("_Count");
        private static readonly int MapBufferPropertyID =       Shader.PropertyToID("_MapBuffer");
        private static readonly int ParticleBufferPropertyID =  Shader.PropertyToID("_ParticleBuffer");
        private static readonly int ParticleSizePropertyID =    Shader.PropertyToID("_ParticleSize");
        private static readonly int DeltaTimePropertyID =       Shader.PropertyToID("_DeltaTime");

        // MONOBEHAVIOURS
        private void Awake()
        {
            CreateBuffers();
            SetArgsBufferData();
            InitializeParticleBuffer();
            InitializeMapBuffer();
        }

        private void Update()
        {
            UpdateMaterial();
            InitializeMapBuffer();
            UpdateMapBuffer();
            UpdateParticleBuffer();
            Draw();
        }

        private void OnDisable() => ReleaseResources();
        private void OnDestroy() => ReleaseResources();
        
        // PRIVATE FUNCTIONS
        /// Instantiate the particle, spacial map and arguments buffers.
        private void CreateBuffers()
        {
            _argsBuffer = new ComputeBuffer(1, _args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
            _mapBuffer = new ComputeBuffer(_mapSize * _mapSize, sizeof(int), ComputeBufferType.Structured);
            _particleBuffer = new ComputeBuffer((int)count, Marshal.SizeOf<Particle>(), ComputeBufferType.Structured);
        }

        /// Get arguments from the mesh and write to args buffer.
        private void SetArgsBufferData()
        {
            if (mesh != null)
            {
                _args[0] = mesh.GetIndexCount(0);
                _args[1] = count;
                _args[2] = mesh.GetIndexStart(0);
                _args[3] = mesh.GetBaseVertex(0);
            }
            else _args[0] = _args[1] = _args[2] = _args[3] = 0;
            _argsBuffer.SetData(_args);
        }
        
        private void InitializeParticleBuffer()
        {
            computeShader.SetBuffer((int)KernelIDs.InitializeParticles, ParticleBufferPropertyID, _particleBuffer);
            computeShader.SetInt(CountPropertyID, (int)count);
            computeShader.SetInt(MapSizePropertyID, _mapSize);
            computeShader.GetKernelThreadGroupSizes((int)KernelIDs.InitializeParticles, out uint x, out uint _, out uint _);
            computeShader.Dispatch((int)KernelIDs.InitializeParticles, (int)(count / x)+1, 1, 1);
        }

        private void InitializeMapBuffer()
        {
            computeShader.SetBuffer((int)KernelIDs.InitializeMap, MapBufferPropertyID, _mapBuffer);
            computeShader.SetInt(MapSizePropertyID, _mapSize);
            computeShader.GetKernelThreadGroupSizes((int)KernelIDs.InitializeMap, out uint x, out uint y, out uint _);
            computeShader.Dispatch((int)KernelIDs.InitializeMap, (int)(_mapSize / x)+1, (int)(_mapSize / y)+1, 1);
        }

        private void UpdateMapBuffer()
        {
            computeShader.SetBuffer((int)KernelIDs.UpdateMap, ParticleBufferPropertyID, _particleBuffer);
            computeShader.SetBuffer((int)KernelIDs.UpdateMap, MapBufferPropertyID, _mapBuffer);
            computeShader.SetInt(CountPropertyID, (int)count);
            computeShader.SetInt(MapSizePropertyID, _mapSize);
            computeShader.GetKernelThreadGroupSizes((int)KernelIDs.UpdateMap, out uint x, out uint _, out uint _);
            computeShader.Dispatch((int)KernelIDs.UpdateMap, (int)(count / x)+1, 1, 1);
        }

        private void UpdateParticleBuffer()
        {
            computeShader.SetBuffer((int)KernelIDs.UpdateParticles, ParticleBufferPropertyID, _particleBuffer);
            computeShader.SetBuffer((int)KernelIDs.UpdateParticles, MapBufferPropertyID, _mapBuffer);
            computeShader.SetInt(CountPropertyID, (int)count);
            computeShader.SetInt(MapSizePropertyID, _mapSize);
            computeShader.SetFloat(DeltaTimePropertyID, Time.deltaTime);
            computeShader.SetFloat(ParticleSizePropertyID, particleSize);

            computeShader.GetKernelThreadGroupSizes((int)KernelIDs.UpdateParticles, out uint x, out uint _, out uint _);
            computeShader.Dispatch((int)KernelIDs.UpdateParticles, (int)(count / x)+1, 1, 1);
        }

        
        
        private void UpdateMaterial()
        {
            material.SetInt(CountPropertyID, (int)count);
            material.SetInt(MapSizePropertyID, _mapSize);

            material.SetBuffer(ParticleBufferPropertyID, _particleBuffer);
            material.SetBuffer(MapBufferPropertyID, _mapBuffer);

            material.SetFloat(ParticleSizePropertyID, particleSize);
        }

        private void Draw()
        {
            Graphics.DrawMeshInstancedIndirect(mesh, 0, material, Bounds, _argsBuffer);
        }

        private void ReleaseResources()
        {
            if (_particleBuffer != null)
            {
                _particleBuffer.Release();
                _particleBuffer = null;
            }

            if (_mapBuffer != null)
            {
                _mapBuffer.Release();
                _mapBuffer = null;
            }

            if (_argsBuffer != null)
            {
                _argsBuffer.Release();
                _argsBuffer = null;
            }
        }
    }
}