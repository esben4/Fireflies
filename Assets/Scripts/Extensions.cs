using Unity.Mathematics;
using UnityEngine;

public static class Extensions
{
    public static float4 ToFloat4(this Color color) => new float4(color.r, color.g, color.b, color.a);
    public static float4 ToFloat4(this Vector4 vector) => new float4(vector.x, vector.y, vector.z, vector.w);
    public static Vector4 ToVector4(this float4 vector) => new float4(vector.x, vector.y, vector.z, vector.w);
}