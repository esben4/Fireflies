using System;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;

namespace PixelSand
{
    public class PixelSandController : MonoBehaviour
    {
        private RenderTexture rt;
        [SerializeField] private ComputeShader cs;
        [SerializeField] private Camera cam;

        enum KernelIDs
        {
            Init = 0,
            UpdateSim = 1,
            UpdateDraw = 2,
            UpdateDelete = 3,
        }
        private void Awake()
        {
            Application.targetFrameRate = 30;
            
            rt = new RenderTexture(512, 512, 0, GraphicsFormat.R8_UNorm);
            rt.enableRandomWrite = true;
            rt.filterMode = FilterMode.Point;
            rt.Create();

            cs.SetTexture((int)KernelIDs.Init, "Result", rt, 0, RenderTextureSubElement.Color);
            cs.GetKernelThreadGroupSizes((int)KernelIDs.Init, out uint x, out uint y, out uint _);
            cs.Dispatch((int)KernelIDs.Init, (int)(rt.width / x), (int)(rt.height / y), 1);

            var mr = GetComponent<MeshRenderer>();
            mr.material.SetTexture("_MainTex", rt);
        }

        private void Update()
        {
            for (int i = 0; i < 4; i++)
            {
                cs.SetTexture((int)KernelIDs.UpdateSim, "Result", rt, 0, RenderTextureSubElement.Color);
                cs.SetInt("_width", rt.width);
                cs.GetKernelThreadGroupSizes((int)KernelIDs.UpdateSim, out uint x, out uint y, out uint _);
                cs.Dispatch((int)KernelIDs.UpdateSim, (int)(rt.width / x), (int)(rt.height / y), 1);
            }

            if (Input.GetMouseButton(0))
            {
                RaycastHit hit;
                if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit))
                    return;

                Vector2 pixelUV = hit.textureCoord;
                pixelUV.x *= rt.width;
                pixelUV.y *= rt.height;

                cs.SetTexture((int)KernelIDs.UpdateDraw, "Result", rt, 0, RenderTextureSubElement.Color);
                cs.SetVector("_drawCoord", new Vector4(pixelUV.x, pixelUV.y, 0f, 0f));
                cs.GetKernelThreadGroupSizes((int)KernelIDs.UpdateDraw, out uint x, out uint y, out uint _);
                cs.Dispatch((int)KernelIDs.UpdateDraw, (int)(rt.width / x), (int)(rt.height / y), 1);
            }
            else if (Input.GetMouseButton(1))
            {
                RaycastHit hit;
                if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit))
                    return;

                Vector2 pixelUV = hit.textureCoord;
                pixelUV.x *= rt.width;
                pixelUV.y *= rt.height;

                cs.SetTexture((int)KernelIDs.UpdateDelete, "Result", rt, 0, RenderTextureSubElement.Color);
                cs.SetVector("_drawCoord", new Vector4(pixelUV.x, pixelUV.y, 0f, 0f));
                cs.GetKernelThreadGroupSizes((int)KernelIDs.UpdateDelete, out uint x, out uint y, out uint _);
                cs.Dispatch((int)KernelIDs.UpdateDelete, (int)(rt.width / x), (int)(rt.height / y), 1);
            }

        }
    }
}