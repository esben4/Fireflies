using System.Runtime.InteropServices;
using Unity.Mathematics;
using UnityEngine;

namespace Boids
{
    public enum BoidsComputeShaderKernels
    {
        MainLoop = 0,
        SortBoids = 1,
        InitializeMap = 2
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Boid
    {
        public float life;
        public int linkedIndex;
        public float2 position;
        public float2 velocity;
        public float4 color;

        public Boid(float2 position, float2 velocity, Color color)
        {
            life = 1f;
            linkedIndex = -1;
            this.position = position;
            this.velocity = velocity;
            this.color = color.ToFloat4();
        }
    }

    public class BoidsController : MonoBehaviour
    {
        [Header("Initialize Properties")] [SerializeField]
        private ComputeShader boidsComputeShader;

        [SerializeField] private Material material;
        [SerializeField] private MeshRenderer displayMeshRenderer;
        [SerializeField] private new Camera camera;
        [SerializeField] private Texture2D circleTexture;
        [SerializeField] private Font guiFont;
        [Space]
        [Space]
        [SerializeField] private int Capacity = 2048;
        [SerializeField] private int MaxIterations = 16;
        [SerializeField] private float PerceptionRadius = 1.0f;
        [SerializeField] private float BoundsSize = 16f;
        [SerializeField] private float SpawnSize = 16f;
        [SerializeField] private uint Seed;
        [SerializeField] private float2 playerStartPosition;
        [Space]
        [Header("Update Properties")]
        [Range(0, 1)] [SerializeField] private float Inertia = .5f;
        [SerializeField] private float Conformity = 1f;
        [SerializeField] private float Convergence = 1f;
        [SerializeField] private float Randomce = 0.5f;
        [SerializeField] private float Divergence = 2f;
        [SerializeField] private float DivergenceFalloff = 3f;
        [SerializeField] private float MaximumVelocity = 1f;
        [SerializeField] private float ControlDistance = 1f;
        [SerializeField] private float ControlFalloff = 3f;
        [SerializeField] private float PlayerImportanceWeight = 4f;

        private ComputeBuffer boidsBuffer;
        private ComputeBuffer cellBuffer;
        private Bounds bounds;
        private int mapSize;
        private int mapSizeMaximum = 2048;
        private int count;

        private Boid player;
        private float inputX;
        private float inputY;
        private float inputZ;
        private Font font;

        private bool debugGuiEnabled;

        private void Awake()
        {
            font = guiFont;
            displayMeshRenderer.transform.localScale = new Vector3(BoundsSize, BoundsSize, BoundsSize);

            for (int i = 0; i < 32; i++)
            {
                int j = 1 << i;

                if (j >= Capacity)
                {
                    Capacity = j;
                    Debug.Log($"Capacity: {Capacity}");
                    break;
                }
            }

            bounds = new Bounds(new float3(0), new float3(BoundsSize));
            int stride = Marshal.SizeOf<Boid>();
            boidsBuffer = new ComputeBuffer(Capacity, stride, ComputeBufferType.Structured);

            mapSize = Mathf.CeilToInt(BoundsSize / ( PerceptionRadius * .5f ));

            for (int i = 0; i < 32; i++)
            {
                int j = 1 << i;

                if (j >= mapSize)
                {
                    mapSize = j;
                    Debug.Log($"mapSize: {mapSize}");
                    break;
                }
            }

            mapSize = math.min(mapSize, mapSizeMaximum);
            
            cellBuffer = new ComputeBuffer( mapSize * mapSize, Marshal.SizeOf<int>());
            
            count = Capacity;
            Boid[] newBoids = new Boid[count];

            var rand = new Unity.Mathematics.Random(Seed);
            float2 globalVelocityBias = rand.NextFloat2(.75f, 1.2f);
            for (int i = 0; i < newBoids.Length; i++)
            {
                //POSITION
                float minSize = math.min(SpawnSize, BoundsSize);
                float2 position = rand.NextFloat2(-minSize * .5f, minSize * .5f);
                
                //VELOCITY
                float polar = math.atan2(position.x, position.y);
                float2 randVel = rand.NextFloat2(-1f, 1f);
                randVel *= randVel * randVel;
                float2 rotationalVel = new float2(math.cos(4f * polar), math.sin(4f * polar));
                float2 velocity = (randVel + .25f * rotationalVel) * globalVelocityBias;
                
                //COLOR
                Color boidColor = UnityEngine.Random.ColorHSV(.25f, .5f, .1f, .8f, .5f, .8f);
                if (i == 0)
                {
                    position = new float2(playerStartPosition);
                    boidColor = new Color(1.0f, 0.3f, 0.1f);
                }
                else if (i < 10 ) boidColor *= 2f * new Color(1.0f, 0.5f, 0.4f);
                else if (i < 100) boidColor *= 2f * new Color(0.6f, 0.4f, 1.0f);

                newBoids[i] = new Boid(position, velocity, boidColor);
            }

            boidsBuffer.SetData(newBoids);
        }

        private void Update()
        {
            if (displayMeshRenderer != null && displayMeshRenderer.material != null)
                UpdateDisplay();
            UpdateInput();
            UpdateBoids();
            UpdateCamera();
        }
        
        private void LateUpdate() => RenderBoids();
        private void OnApplicationQuit() => ReleaseResources();
        private void OnDestroy() => ReleaseResources();

        private void UpdateInput()
        {
            Boid[] data = new Boid[1];
            boidsBuffer.GetData(data, 0, 0, 1);
            player = data[0];
            inputX = Input.GetAxis("Horizontal");
            inputY = Input.GetAxis("Vertical");
            inputZ = Input.GetAxis("Zoom");

            if (Input.GetKeyDown("z"))
                ToggleKeyword(displayMeshRenderer.material, "DEBUG_Z");

            if (Input.GetKeyDown("x"))
                ToggleKeyword(displayMeshRenderer.material, "DEBUG_X");

            if (Input.GetKeyDown("c"))
                ToggleKeyword(displayMeshRenderer.material, "DEBUG_C");

            if (Input.GetKeyDown("`"))
                debugGuiEnabled = !debugGuiEnabled;
        }
        
        private void UpdateCamera()
        {
            Boid[] data = new Boid[1];
            boidsBuffer.GetData(data, 0, 0, 1);
            Boid boid = data[0];
            Vector3 p = camera.transform.position;
            p.x = boid.position.x;
            p.y = boid.position.y;
            camera.transform.position = p;
            camera.fieldOfView *= 1f + inputZ * Time.deltaTime;
        }
        
        private void UpdateDisplay()
        {
            displayMeshRenderer.material.SetBuffer("CellBuffer", cellBuffer);
            displayMeshRenderer.material.SetInt("_MapSize", mapSize);
            displayMeshRenderer.material.SetFloat("_BoundsSize", BoundsSize);
            displayMeshRenderer.material.SetVector("_PlayerVec", new float4(player.position, player.position));
        }

        private void UpdateBoids()
        {
            boidsComputeShader.SetInt("_Count", count);
            boidsComputeShader.SetInt("_MaxIterations", MaxIterations);
            boidsComputeShader.SetInt("_Frame", Time.frameCount);
            boidsComputeShader.SetInt("_MapSize", mapSize);
            boidsComputeShader.SetFloat("_DeltaTime", Time.deltaTime);
            boidsComputeShader.SetFloat("_Time", Time.time);

            boidsComputeShader.SetFloat("_BoundsSize", BoundsSize);

            boidsComputeShader.SetFloat("_PerceptionRadius", PerceptionRadius);
            boidsComputeShader.SetFloat("_Inertia", Inertia);
            boidsComputeShader.SetFloat("_InertiaDeltaLerp", 1f - math.pow(Inertia, Time.deltaTime));
            boidsComputeShader.SetFloat("_Conformity", Conformity);
            boidsComputeShader.SetFloat("_Convergence", Convergence);
            boidsComputeShader.SetFloat("_Random", Randomce);
            boidsComputeShader.SetFloat("_Divergence", Divergence);
            boidsComputeShader.SetFloat("_DivergenceFalloff", DivergenceFalloff);
            boidsComputeShader.SetFloat("_MaximumVelocity", MaximumVelocity);

            boidsComputeShader.SetFloat("_ControlDistanceRCP", 1f/ControlDistance);
            boidsComputeShader.SetFloat("_ControlFalloff", ControlFalloff);
            boidsComputeShader.SetFloat("_PlayerImportanceWeight", PlayerImportanceWeight);

            boidsComputeShader.SetVector("_Input", new Vector4(inputX, inputY, 0f, 0f));
            {
                int kernelIndex = (int)BoidsComputeShaderKernels.InitializeMap;
                boidsComputeShader.SetBuffer(kernelIndex, "CellBuffer", cellBuffer);
                boidsComputeShader.GetKernelThreadGroupSizes(kernelIndex, out uint ux, out uint uy, out _);
                int groupsX = mapSize / (int)ux;
                int groupsY = mapSize / (int)uy;
                boidsComputeShader.Dispatch(kernelIndex, groupsX, groupsY, 1);
            }

            {
                int kernelIndex = (int)BoidsComputeShaderKernels.SortBoids;
                boidsComputeShader.SetBuffer(kernelIndex, "BoidBuffer", boidsBuffer);
                boidsComputeShader.SetBuffer(kernelIndex, "CellBuffer", cellBuffer);
                boidsComputeShader.GetKernelThreadGroupSizes(kernelIndex, out uint ux, out _, out _);
                int groupsX = boidsBuffer.count / (int)ux;
                boidsComputeShader.Dispatch(kernelIndex, groupsX, 1, 1);
            }

            {
                int kernelIndex = (int)BoidsComputeShaderKernels.MainLoop;
                boidsComputeShader.SetBuffer(kernelIndex, "BoidBuffer", boidsBuffer);
                boidsComputeShader.SetBuffer(kernelIndex, "CellBuffer", cellBuffer);
                
                boidsComputeShader.GetKernelThreadGroupSizes(kernelIndex, out uint ux, out _, out _);
                int groupsX = boidsBuffer.count / (int)ux;
                boidsComputeShader.Dispatch(kernelIndex, groupsX, 1, 1);
            }
        }

        private void RenderBoids()
        {
            int boidsCount = math.min(count, boidsBuffer.count);
            int vertexCount = 3 * boidsCount;

            material.SetBuffer("BoidBuffer", boidsBuffer);
            Graphics.DrawProcedural(material, bounds, MeshTopology.Triangles, vertexCount);
        }

        private void ReleaseResources()
        {
            boidsBuffer.Dispose();
            cellBuffer.Dispose();
        }
        
        private void ToggleKeyword(Material material, string keyword)
        {
            if (material.IsKeywordEnabled(keyword)) 
                material.DisableKeyword(keyword);
            else
                material.EnableKeyword(keyword);
        }


        private void OnGUI()
        {
            if (!debugGuiEnabled) return;
            GUI.skin.font = font;
            GUILayout.Window(
                id: 0,
                screenRect: new Rect(0, 0f, 420, 256),
                func: _ => {
                    GUILayout.BeginHorizontal();
                    Color r = new Color(1.0f, 0.3f, 0.4f);
                    Color g = new Color(0.7f, 1.0f, 0.3f);
                    Color b = new Color(0.4f, 0.5f, 1.0f);
                    Color k = new Color(0f, 0f, 0f, 0.2f);
                    
                    GUI.color = r;
                    GUILayout.Label($"input: \nx = {inputX: 0.00;-0.00} \ny = {inputY: 0.00;-0.00} \nz = {inputZ: 0.00;-0.00}");
                    GUI.color = g;
                    GUILayout.Label($"position: \nx = {player.position.x: 0.00;-0.00} \ny = {player.position.y: 0.00;-0.00}");
                    GUI.color = b;
                    GUILayout.Label($"velocity: \nx = {player.velocity.x: 0.00;-0.00} \ny = {player.velocity.y: 0.00;-0.00}");
                    GUILayout.EndHorizontal();
                    
                    GUI.color = k;
                    GUI.DrawTexture(new Rect (146, 90, 128, 128), circleTexture);
                    GUI.DrawTexture(new Rect (206, 150, 8, 8), circleTexture);
                    GUI.color = r;
                    GUI.DrawTexture(new Rect (194+inputX*32f, 138-inputY*32f, 32, 32), circleTexture);
                    GUI.color = b;
                    GUI.DrawTexture(new Rect (194+player.velocity.x*32f, 138-player.velocity.y*32f, 32, 32), circleTexture);
                },
                text: "Player"
            );
        }
    }
}