Shader "Unlit/IntMapDisplay"
{
    Properties {}
    SubShader
    {
        Tags
        {
            "RenderType"="Opaque"
        }
        LOD 100

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vertex
            #pragma fragment fragment

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Assets/Shaders/Library/BoidsLib.hlsl"


            struct Attributes
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct Varyings
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            StructuredBuffer<int> CellBuffer;
            float _MapSize, _BoundsSize;

            Varyings vertex(Attributes IN)
            {
                Varyings OUT;
                OUT.vertex = TransformObjectToHClip(IN.vertex.xyz);
                OUT.uv = IN.uv;
                return OUT;
            }

            float4 _PlayerVec;

            float4 fragment(Varyings IN) : SV_Target
            {
                float4 OUT = 1;
                int2 coord = int2(IN.uv * _MapSize);
                OUT.rgb = (uint)(coord.x + coord.y) % 2 == 0 ? float3(.275, .275, .275) : float3(.3, .3, .3);
                
                #define CELL_DEBUG
                #ifdef CELL_DEBUG
                int cell = CellBuffer[ GetIndex(coord, _MapSize) ];
                OUT.rgb += cell == -1 ? .0 : float3(.05, .0, .0);
                #endif

                OUT.r = .5-distance(_PlayerVec.xy,(IN.uv-.5) * _BoundsSize);

                return OUT;
            }
            ENDHLSL
        }
    }
}