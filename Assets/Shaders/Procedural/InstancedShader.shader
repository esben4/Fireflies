Shader "Instanced/InstancedShader"
{
    Properties
    { }

    SubShader
    {
        Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalRenderPipeline" }

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 4.5

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"            

            struct Attributes
            {
                float4 positionOS   : POSITION;
                uint instanceID     : SV_InstanceID;
            };

            struct Varyings
            {
                float4 positionHCS  : SV_POSITION;
                uint instanceID     : SV_InstanceID;
            };            

            float _count;
            Varyings vert(Attributes IN)
            {
                Varyings OUT;
                OUT.positionHCS = TransformObjectToHClip(IN.positionOS.xyz + float3(0,0,32.*(float)IN.instanceID/_count));
                OUT.instanceID = IN.instanceID;
                return OUT;
            }

            
            half4 frag(Varyings IN) : SV_Target
            {
                half4 customColor;
                customColor = half4(1.-(float)IN.instanceID/_count, 0, 0, 1);
                return customColor;
            }
            ENDHLSL
        }
    }
}