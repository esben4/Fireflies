Shader "Procedural/Boids"
{
    Properties
    {
        [MainTex] _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            HLSLPROGRAM
            
            #pragma vertex vertex
            #pragma fragment fragment
            
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Assets/Shaders/Library/BoidsStructs.hlsl"
            
            struct Attributes
            {
                uint vID : SV_VertexID;
            };

            struct Varyings
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float4 color : TEXCOORD1;
            };

            StructuredBuffer<Boid> BoidBuffer;
            
            CBUFFER_START(UnityPerMaterial)
            sampler2D _MainTex;
            float4 _MainTex_ST;
            CBUFFER_END

            static float2 offsets[3] =
            {
                float2(-0.125f, 0.0675f),
                float2( 0.250f, 0.0000f),
                float2(-0.125f,-0.0675f),
            };

            Varyings vertex (Attributes IN)
            {
                Varyings OUT;

                uint boidIndex = IN.vID / 3;
                uint boidVertexIndex = IN.vID % 3;
                Boid boid = BoidBuffer[boidIndex];
                bool isAlive = boid.life>0.0f;
                float2 direction = normalize(boid.velocity.xy);
                float2 origin = boid.position;
                float2 offset = offsets[boidVertexIndex] * .125;
                offset = mul(offset, float2x2(direction.x,direction.y,-direction.y,direction.x));
                float2 vertexPositionOS = origin + offset;

                
                OUT.vertex = isAlive ? TransformObjectToHClip(float3(vertexPositionOS,0)) : 0;
                OUT.uv = TRANSFORM_TEX(offset, _MainTex);
                OUT.color = boid.color;
                return OUT;
            }
            
            float4 fragment (Varyings IN) : SV_Target
            {
                return IN.color;
            }
            
            ENDHLSL
        }
    }
}