Shader "Instanced/Sand"
{
    Properties
    { }

    SubShader
    {
        Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalRenderPipeline" }

        Pass
        {
            Cull Off //  HERE IS WHERE YOU PUT CULL OFF
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 4.5

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"            
            #include "Assets/Shaders/Library/SandLib.hlsl"
            
            struct Attributes
            {
                float4 positionOS   : POSITION;
                uint instanceID     : SV_InstanceID;
            };

            struct Varyings
            {
                float4 positionHCS  : SV_POSITION;
                uint instanceID     : SV_InstanceID;
                half4 color         : COLOR;
            };
            
            int _Count;
            int _MapSize;
            
            StructuredBuffer<Particle> _ParticleBuffer;
            StructuredBuffer<int> _CellBuffer;

            float _ParticleSize;

            Varyings vert(Attributes IN)
            {
                Varyings OUT;
                Particle particle = _ParticleBuffer[IN.instanceID];
                OUT.positionHCS = TransformObjectToHClip(IN.positionOS.xyz * _ParticleSize + float3(particle.position,0));
                OUT.instanceID = IN.instanceID;
                OUT.color = particle.flags == 0 ? float4(1,0,0,1) : float4(0,1,0,1);
                return OUT;
            }

            
            half4 frag(Varyings IN) : SV_Target
            {
                half4 customColor = IN.color;
                return customColor;
            }
            ENDHLSL
        }
    }
}