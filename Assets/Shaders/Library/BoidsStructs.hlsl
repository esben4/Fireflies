#ifndef BOIDS_STRUCTS
#define BOIDS_STRUCT

struct Boid
{
    float life;
    int linkedIndex;
    float2 position;
    float2 velocity;
    float4 color;
};

#endif