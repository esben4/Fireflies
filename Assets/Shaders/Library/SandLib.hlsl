#ifndef SAND_LIB
#define SAND_LIB
struct Particle
{
    int linkedIndex;
    uint flags;
    float2 position;
    float2 velocity;
};
#endif