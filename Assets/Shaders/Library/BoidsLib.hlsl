#ifndef BOIDS_LIB
#define BOIDS_LIB


int GetIndex(int2 coord, int mapSize)
{
    return coord.y * mapSize + coord.x;
}

float2 ISNMinMax(float2 position, float margin = 0.0625)
{
    return (position - min(1 - margin, max(margin - 1, position))) / margin;
}

float2 BoundryRepulsion(float2 position, float2 bounds, float power, float perceptionRadius, float boundsSize)
{
    return sign(position) * pow(abs(ISNMinMax(position / (.5 * bounds), 2. * perceptionRadius / boundsSize)), power);
}

float2 BoundConstrain(float2 position, float2 bounds)
{
    float2 halfBounds = .5 * bounds;
    return min(halfBounds, max(-halfBounds, position));
}

int2 GetCell(float2 position, float boundsSize, int _MapSize) //_BoundsSize, _MapSize)
{
    return (int2)((position / boundsSize + .5) * _MapSize);
}

float2 rand2(in float2 uv)
{
    float2x2 randMatrix = float2x2(531.4507, 944.3374, 854.0097, 702.4028);
    float2 U = sin(mul(uv, randMatrix));
    return -sin(U * 43758.54153);
}

float atan2F2(float2 vec)
{
    return atan2(vec.x, vec.y);
}


#endif
